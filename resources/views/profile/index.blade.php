@extends('layouts.dashboard')

@section('breadcrumbs', 'Perfil')

@section('content')

<div class="row">
	<div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
		<div class="card card-profile shadow">
			<div class="row justify-content-center">
				<div class="col-lg-3 order-lg-2">
					<div class="card-profile-image">
						<a href="#">
							<img src="{{ empty(Auth::user()->url) ? asset('images/no-profile.jpg') : Auth::user()->url }}" class="rounded-circle">
						</a>
					</div>
				</div>
			</div>
			<div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
				<div class="d-flex justify-content-between"></div>
			</div>
			<div class="card-body pt-0 pt-md-4">
				<div class="row">
					<div class="col">
						<div class="card-profile-stats d-flex justify-content-center mt-md-5"></div>
					</div>
				</div>
				<div class="text-center">
					<h3>{{ $user->names }}, {{ $user->surnames }}</h3>
					<div class="h5 font-weight-300">
							<i class="ni location_pin mr-2"></i>{{ $user->email }}
					</div>
					<div class="h5 mt-4 text-uppercase">
							<i class="ni business_briefcase-24 mr-2"></i> {{ $user->type }}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-8 order-xl-1">

		@include('errors.messages')
		
		<div class="card bg-secondary shadow">
			<div class="card-header bg-white border-0">
				<div class="row align-items-center">
					<div class="col-8">
							<h3 class="mb-0">Mi Perfil</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<profile :user="{{ $user }}"></profile>
			</div>
		</div>
	</div>
</div>

@endsection