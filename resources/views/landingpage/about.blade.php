@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
    <div class="row">

			<div class="container">

				<div class="col-xl-12">
						<img src="{{ asset('/images/titulo_nosotros.png') }}" alt="" width="530px">
				</div>

				<p class="text-justify">
					<strong>BLINS</strong> es uno de los centros de operaciones de la RED NACIONAL DE SEGURIDAD con proyección a nivel nacional. Nuestro anhelo es estar cada vez más cerca de nuestros Clientes, ofreciendo una gama de soluciones integrales a un mercado exigente y altamente sofisticado.
				</p>
				
				<p class="text-justify">
					<strong>EL OBJETIVO:</strong> Ser el mejor proveedor de sistemas y servicios de protección, brindado tranquilidad a los procesos críticos de nuestros Clientes.
				</p>
				
				<p class="text-justify">Nuestro éxito se mide por el valor agregado que proporcionamos a quienes deciden integrarnos como su gente  de confianza. Somos una Empresa que se esfuerza por establecer condiciones beneficiosas a los Clientes, Proveedores, Empleados y Accionistas, para así implementar programas que se ajusten a políticas de calidad de sus servicios y mejora.</p>
				
				<p class="text-justify">Nuestra vocación es la de integrarnos a todas las comunidades a través de un ejercicio profesional, ético e idóneo.</p>
				
				<p class="text-justify">Nuestra filosofía empresarial es la de fomentar la producción y eficiencia en un entorno digno y de respeto. Asumimos el compromiso de ser exitosos tanto en lo personal como en el ámbito empresarial.</p>
				
				<p class="text-justify">Sostenemos los principios de integridad, vocación de servicio, valoración de las personas y respeto por la tradición de la empresa, que nos caracterizan en nuestro que hacer cotidiano y  a través de los cuales nuestra Empresa cobra un valor especial “Satisfacción de nuestros Clientes y Amigos”, generando fuentes de empleo, tranquilidad y confianza.</p>

				<h2 class="my-5">
					NUESTROS SERVICIOS:
				</h2>

				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
							<p class="text-left">Asesoramiento & Consultoría</p>	
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
							<p class="text-left">Auditorías</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					<p class="text-left">Protección PBIP</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
							<p class="text-left">Soporte Técnico</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					<p class="text-left">Circuito Cerrado de TV (Local, Red, Internet y Móvil Celular)</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					<p class="text-left">Central de Monitoreo de Alarmas</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					<p class="text-left">Sistemas de Alarmas</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					<p class="text-left">Localización y Control por GPS</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					<p class="text-left">Central de Monitoreo de Imágenes</p>
				</div>
				<div class="col">
					<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
						<p class="text-left">Asesoramiento para la Capacitación y Entrenamiento del Personal de Seguridad </p>
				</div>
			</div>
		</div>

</div>

@endsection
