@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
    <div class="row">

		<div class="col-xl-12">
			<img src="{{ asset('/images/titulo_videos.png') }}" alt="" width="530px">
		</div>
		<div class="col">
			<div>

				<videos-interest></videos-interest>
			</div>
		</div>
	</div>	
</div>

@endsection