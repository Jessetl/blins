@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/login_fondo.jpg') }}); background-size: cover;">
    <div class="row justify-content-center">
        <div class="col-xl-12">
            <img src="{{ asset('/images/titulo_login.png') }}" alt="" width="530px">
        </div>
        <div class="col-xl-4">
            <div class="card shadow my-5" style="box-shadow: 0px 0px 15px 1px #c0c0c0 !important;">
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-email">Correo electrónico</label>
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input type="text" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus/>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-password">Contraseña</label>
                                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <input type="password" id="input-password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required/>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            Recuérdame
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary" style="background-image: url({{ asset('images/fondo_boton.png')}});background-color: transparent;background-position: center;background-size: 120% 162%;">
                                        Enviar
                                    </button>
                                </div>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    ¿Se te olvidó tu contraseña?
                                </a>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        ¿Necesito una cuenta? <a href="{{ route('register') }}">Registrarse.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
