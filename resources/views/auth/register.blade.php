@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-6 order-xl-1">
            <div class="card shadow">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Registrarme</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf  
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-names">Nombres</label>
                                    <div class="form-group{{ $errors->has('names') ? ' has-danger' : '' }}">
                                        <input type="text" id="input-names" class="form-control{{ $errors->has('names') ? ' is-invalid' : '' }}" name="names" placeholder="Damian" required autofocus/>

                                        @if ($errors->has('names'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('names') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-surnames">Apellidos</label>
                                    <div class="form-group{{ $errors->has('surnames') ? ' has-danger' : '' }}">
                                        <input type="text" id="input-surnames" class="form-control{{ $errors->has('surnames') ? ' is-invalid' : '' }}" name="surnames" placeholder="Pérez" required/>

                                        @if ($errors->has('surnames'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('surnames') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-email">Correo electrónico</label>
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input type="text" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="example@mail.com" required/>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-password">Contraseña</label>
                                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <input type="password" id="input-password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required/>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-control-label" for="input-password_confirmation">Confirmar contraseña</label>
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                        <input type="password" id="input-password_confirmation" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required/>

                                        @if ($errors->has('password_confirmation'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="pl-lg-4">
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Registrarme
                                    </button>
                                </div>
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    Ya tengo una cuenta!
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
