@extends('layouts.dashboard')

@section('breadcrumbs', 'Editar noticias')

@section('content')

<div class="row justify-content-center">
	<div class="col-xl-8 order-xl-1">
		<div class="card bg-secondary shadow">
			<div class="card-header bg-white border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Editar noticia</h3>
					</div>
				</div>
			</div>
			{{--<edit-news :article="{{ $new }}" :http="'{{ route('news.update', $new) }}'"></edit-news>--}}
            <div class="card-body">
                <form ref="form" id="createform" action="{{ route('news.update', $article) }}" novalidate enctype="multipart/form-data" method="POST" autocomplete="off">
                    @method('PUT')
                    @csrf
                    <h6 class="heading-small mb-4">Información de artículo</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="form-control-label" for="input-title">Título</label>
                                <div class="form-group {{ $errors->has('title') ? 'has-danger' : '' }}">
                                    <input
                                        type="text"
                                        id="input-title"
                                        name="title"
                                        class="form-control {{ $errors->has('title') ? 'has-danger' : '' }}"
                                        placeholder="Blins"
                                        value="{{ $article->title }}"
                                        required/>
                                    @if ($errors->has('title'))
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-control-label" for="input-date_ini">Fecha Inicio</label>
                                <div class="form-group @if($errors->has('date_ini')) has-danger @endif">
                                    <input type="date" id="input-date_ini" name="date_ini" value="{{ $article->date_ini }}" class="form-control @if($errors->has('date_ini')) is-invalid @endif" required/>
                                    @if ($errors->has('date_ini'))
                                          <span class="text-danger">{{ $errors->first('date_ini') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-control-label" for="input-date_end">Fecha Final</label>
                                <div class="form-group @if($errors->has('date_end')) has-danger @endif">
                                    <input type="date" id="input-date_end" name="date_end" value="{{ $article->date_end }}" class="form-control @if($errors->has('date_end')) is-invalid @endif" required/>
                                    @if ($errors->has('date_end'))
                                          <span class="text-danger">{{ $errors->first('date_end') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pl-lg-4">
                        <label class="form-control-label" for="detail">Detalles</label>
                        <div class="form-group {{ $errors->has('detail') ? 'has-danger' : '' }}">
                            <textarea
                                rows="4"
                                id="detail"
                                name="detail"
                                class="form-control {{ $errors->has('detail') ? 'has-danger' : '' }}"
                                placeholder="Características del artículo ...">
                                {{ $article->detail }}
                            </textarea>
                            @if ($errors->has('detail'))
                                <span class="text-danger">{{ $errors->first('detail') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="pl-lg-4">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                                @if ($errors->has('image'))
                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="pl-lg-4">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>
</div>

@endsection
