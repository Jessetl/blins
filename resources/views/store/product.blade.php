@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid">
	<div class="row">	
		<div class="col-xl-4 order-xl-2 mb-5 mb-xl-3">
			<div class="card">
				<div class="card-body pt-0 pt-md-4">
					<div class="row">
						<div class="card-profile-stats d-flex justify-content-center w-100">
	                    	<div>
	                      		<span class="heading">{{ $product->fee }} $</span>
	                      		<span class="description">Precio</span>
	                    	</div>
		                    <div>
		                      	<span class="heading">{{ $product->feeresale }} $</span>
		                      	<span class="description">Alquiler</span>
		                    </div>
		                    <div>
		                      	<span class="heading">{{ $product->offer ? $product->feeoffer : 0 }} $</span>
		                      	<span class="description">Oferta</span>
		                    </div>
	                  	</div>
						<div class="text-center">
							<h3>
								{{ $product->name }}
							</h3>
							<div class="h5 font-weight-300">
								{{ $product->category->name }}
							</div>

							<hr class="my-4">

							<p>
								{{ $product->detail }}
							</p>
						</div>

						@if ($product->features->count() > 0)
						<div class="col">

							<hr class="my-4">

							<h3 class="my-3">
								Características:
							</h3>

							@foreach($product->features as $key => $feature)

								<p>
									- {{ $feature->detail }}	
								</p>

							@endforeach

						</div>
						@endif
					</div>
				</div>
				<div class="card-footer">
					<a href="{{ route('cart.add', $product) }}" class="btn btn-sm btn-primary">
						AÑADIR AL CARRITO
					</a>
				</div>
			</div>
		</div>
		<div class="col-xl-8 order-xl-1">
			<div class="card">
				<div class="card-header">
					Galeria
				</div>
				<div class="card-body">
					<div class="row justify-content-center">
						
						<img src="{{ asset('storage/' . $product->image) }}" class="img-fluid">

						@foreach($product->images as $key => $image)

							<img src="{{ asset('storage/' . $image->image) }}" class="img-fluid">

						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection