@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
    <div class="row">

			<div class="col-xl-12">
				{{-- <img src="{{ asset('/images/titulo_categorias.png') }}" alt="" width="530px"> --}}
				<h3>FORMAS DE PAGO</h3>
			</div>

			<div class="col-md-12">
				<div class="alert alert-primary">
					<p>Si desea cualquier información sobre nuestras formas de pago envíenos un correo a <a class="alert-link" href="mailto:info@blins.com.uy">info@blins.com.uy</a></p>
				</div>
			</div>
		
			@foreach($payments_methods as $key => $value)

			<div class="col-12 co-sm-6 col-md-3 my-5" >
				<div class="card" style="box-shadow: 0px 0px 5px 1px #727272;padding: 10px">
						<img class="card-img-top" src="{{ empty($value->image) ? asset('/images/logo.png') : asset('/storage/' . $value->image) }}" alt="{{ $value->name }}" height="100">
						<div class="card-body" style="text-align: center;">
							<h5 class="card-title">{{ $value->name }}</h5>
						</div>
				</div>
			</div>

			@endforeach

	</div>

	
</div>

@endsection