@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
    <div class="row">
			<div class="col-xl-12">
				<img src="{{ asset('/images/titulo_ofertas.png') }}" alt="" width="530px">
			</div>
			<div class="col-xl-11">
				<offers-products></offers-products>	
			</div>
			<div class="col-xl-1">
				<img src="{{ asset('/images/oferta_label.png') }}" alt="" width="width: 191px;">
			</div>
		</div>
	</div>
</div>

@endsection