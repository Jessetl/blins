@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
    <div class="row">
			<div class="col-xl-12">
				<img src="{{ asset('/images/titulo_categorias.png') }}" alt="" width="530px">
			</div>
		
			@foreach($categories as $key => $category)

			<div class="col-12 co-sm-6 col-md-3 my-5" >
				<div class="card" style="box-shadow: 0px 0px 5px 1px #727272;">
						<img class="card-img-top" src="{{ empty($category->image) ? asset('/images/logo.png') : asset('/storage/' . $category->image) }}" alt="{{ $category->name }}" height="195">
						<div class="card-body" style="text-align: center;">
							<h5 class="card-title">{{ $category->name }}</h5>
							<a href="{{ route('store', $category) }}" class="btn btn-primary btn-sm" style="background-image: url({{ asset('images/fondo_boton.png')}});background-color: transparent;background-position: center;background-size: 120% 162%;">VER PRODUCTOS</a>
						</div>
				</div>
			</div>

			@endforeach

	</div>	
</div>

@endsection