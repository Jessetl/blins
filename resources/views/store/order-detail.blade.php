@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">

	@include('errors.messages')
	
	<div class="table-cart">
		<div class="table-responsive">
			<h3 class="my-3">
				DATOS DEL USUARIO
			</h3>
	    	<table class="table align-items-center table-flush">
	        	<thead class="thead-gray">
	          		<tr style="background-color: #7ec1df;">
	            		<th scope="col">Nombres</th>
	            		<th scope="col">{{ Auth::user()->names }}</th>
	          		</tr>
	          		<tr style="background-color: #7ec1df;">
	            		<th scope="col">Apellidos</th>
	            		<th scope="col">{{ Auth::user()->surnames }}</th>
	          		</tr>
	          		<tr style="background-color: #7ec1df;">
	            		<th scope="col">Correo electrónico</th>
	            		<th scope="col">{{ Auth::user()->email }}</th>
	          		</tr>
	        	</thead>
	        	<tbody>
	        	</tbody>
	        </table>
	    </div>
		<div class="table-responsive">
			<h3 class="my-3">
				DATOS DEL PEDIDO
			</h3>
	    	<table class="table align-items-center table-flush">
	        	<thead class="thead-gray">
	          		<tr style="background-color: #dad8dd;">
	            		<th scope="col">Producto</th>
	            		<th scope="col">Precio</th>
	            		<th scope="col">Cantidad</th>
	            		<th scope="col">Subtotal</th>
	          		</tr>
	        	</thead>
	        	<tbody>
	        		@forelse($cart as $key => $item)
	        		<tr style="background-color: #7ec1df;">
	        			<th>
	                        {{ $item->name }}
	                    </th> 
	                    <th>
	                    	{{ $item->offer ? $item->feeoffer : $item->fee }}
	                    </th>
	                    <th>
	                    	{{ $item->quantity }}
	                    </th>
	                    <th>
	                    	{{ $item->offer ? number_format($item->feeoffer * $item->quantity, 2) : number_format($item->fee * $item->quantity, 2) }}
	                    </th>
	        		</tr>	
	        		@empty
	    			<tr>
	    				<td colspan="6">No hay productos añadidos al carrito.</td>
	    			</tr>
	        		@endforelse
	    		</tbody>
	        </table>

	        <h3 class="text-center">
	        	<span class="badge badge-success">
	        		Total: {{ number_format($total, 2) }}
	        	</span>
	        </h3>

	        <hr>

	        <p class="text-center">
	        	<a href="{{ route('cart.show') }}" class="btn btn-primary">
	        		Regresar
	        	</a>
	        	<a href="{{ route('payment') }}" class="btn btn-primary">
	        		Pagar con PayPal
	        	</a>
	        </p>
	    </div>
	</div>
</div>

@endsection