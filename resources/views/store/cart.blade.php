@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
	
	@include('errors.messages')

	<div class="table-cart">
		<p>
			<a href="{{ route('cart.trash') }}" class="btn btn-danger">
				Vaciar carrito <i class="fa fa-trash"></i>
			</a>			
		</p>
		<div class="table-responsive">
	    	<table class="table align-items-center table-flush">
	        	<thead class="thead-gray">
	          		<tr style="background-color: #dad8dd;">
	            		<th scope="col">Imagen</th>
	            		<th scope="col">Producto</th>
	            		<th scope="col">Precio</th>
	            		<th scope="col">Cantidad</th>
	            		<th scope="col">Subtotal</th>
	            		<th scope="col">Quitar</th>
	          		</tr>
	        	</thead>
	        	<tbody >
	        		@forelse($cart as $key => $item)
	        		<tr style="background-color: #7ec1df;">
	        			<td><img src="{{ asset('storage/products/' . $item->image) }}"></td>
	        			<th>
	                        {{ $item->name }}
	                    </th> 
	                    <th>
	                    	{{ $item->offer ? $item->feeoffer : $item->fee }}
	                    </th>
	                    <th>
	                    	<input type="number" min="1" max="100" value="{{ $item->quantity }}" id="product_{{ $item->id }}" />
	                    	<a href="#" class="btn btn-warning btn-sm btn-update-item" data-href="{{ route('cart.update', [$item->slug, null]) }}" data-id="{{ $item->id }}">
	                    		<i class="fa fa-redo"></i>
	                    	</a>
	                    </th>
	                    <th>
	                    	{{ $item->offer ? number_format($item->feeoffer * $item->quantity, 2) : number_format($item->fee * $item->quantity, 2) }}
	                    </th>
	                    <th class="text-center">
	                    	<a href="{{ route('cart.delete', $item->slug) }}" class="btn btn-sm btn-danger">
	                    		<i class="fa fa-minus"></i>
	                    	</a>
	                    </th>
	        		</tr>	
	        		@empty
	    			<tr>
	    				<td colspan="6">No hay productos añadidos al carrito.</td>
	    			</tr>
	        		@endforelse
	    		</tbody>
	        </table>

	        <h3 class="text-center">
	        	<span class="badge badge-success">
	        		Total: {{ number_format($total, 2) }}
	        	</span>
	        </h3>
	    </div>
	    
	    <hr>

	    <p class="text-center">
	    	<a href="{{ route('store.category') }}" class="btn btn-primary">
	    		Ir a la tienda
	    	</a>
	    	<a href="{{ route('order.detail') }}" class="btn btn-primary">
	    		Continuar
	    	</a>
	    </p>

	</div>
</div>

@endsection


@section('scripts')

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
// Update item cart

$( document ).ready(function() {
	$(".btn-update-item").on('click', function (e) {

		e.preventDefault();
		
		var id = $(this).data('id');
		var href = $(this).data('href');
		var quantity = $("#product_" + id).val();

		window.location.href = href + "/" + quantity;
	});
});


</script>

@endsection