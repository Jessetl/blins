@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8 pb-4" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
	<div class="row">
		<div clas="col-md-12" style="width: 100%">
			<h1 class="text-uppercase">
				{{ $category->name }}
			</h1>
		</div>
		<div clas="col-md-12">
			<category-products :category="{{ $category->id }}"></category-products>
		</div>
	</div>
</div>

@endsection