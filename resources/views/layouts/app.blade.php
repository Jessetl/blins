<!DOCTYPE html> <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('/images/favicon.png') }}" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="{{ asset('nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet">
    <style type="text/css" media="screen">
    	#whatsapp{
    		position: fixed;
    		bottom: 70px;
    		right: 10px;
    	}
        #ft1{
            padding-bottom: 72px;
        }
        #navbar-default{
            position: relative;
            bottom: 44px;
        }
        #logo{
            position: absolute;
            top: 0px;
            left: 6rem;
            background-color: rgba(255,255,255,.5);
            max-width: 12rem;
        }
        @media (max-width:768px) {
            #logo{
                left: 2rem;
                max-width: 10rem;
            }
            #navbar-default{
                position: relative;
                top: 44px;
            }
            #ft1{
                padding-bottom: 45px;
            }
            #whatsapp{
				bottom: 95px;
    			right: 25px;
            }

        }
        @media (max-width: 568px){
            #ft1{
                padding-bottom: 8px;
            }
            
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-transparent navbar-expand-lg navbar-dark" style="background-image: url({{ asset('/images/banner_menu_new.png') }})">
        <div class="container-fluid">
            <div class="logo" id="logo">
                <img  src="{{ asset('/images/logo_header_new.png') }}" class="img-fluid">
            </div>
            <a class="navbar-brand d-md-block d-lg-none" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon" style=" right: 1rem; position: absolute; top: 6rem;"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-default" style="">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="{{ url('/') }}">
                                <img src="{{ asset('logo.png') }}">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
                
                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ url('/') }}">
                            INICIO
                            <span class="sr-only">(current)</span>                        
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link nav-link-icon" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            EMPRESA
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                            <a class="dropdown-item" href="{{ route('about') }}">Nosotros</a>
                            {{--<a class="dropdown-item" href="{{ route('services') }}">Servicios</a>--}}
                            <a class="dropdown-item" href="{{ route('links') }}">Enlaces de interes</a>
                            <a class="dropdown-item" href="{{ route('videos') }}">Videos de interes</a>
                            <a class="dropdown-item" href="#">Testimonios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link nav-link-icon" href="#" id="navbar-default_dropdown_2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            TIENDA
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_2">
                            <a class="dropdown-item" href="{{ route('cart.show') }}">Carrito</a>
                            <a class="dropdown-item" href="{{ route('store.category') }}">Productos</a>
                            <a class="dropdown-item" href="{{ route('offers') }}">Ofertas</a>
                            <a class="dropdown-item" href="{{ route('payment_methods') }}">Formas de Pagos</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('articles.index') }}">
                            NOTICIAS                      
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('bags.index') }}">
                            BOLSA LABORAL                      
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('message.create') }}">
                            CONTACTO                      
                        </a>
                    </li>
                    @guest
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('login') }}">
                            ACCEDER                      
                        </a>
                    </li>
                    @else
                    @if(Auth::user()->type == 'root' OR Auth::user()->type == 'administrator')
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('home') }}">
                            ADMINISTRACIÓN                        
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            SALIR                      
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    @yield('headerslider')

    <div id="app">
            
        @yield('content')

    </div>
    <section id="whatsapp"><a href="https://api.whatsapp.com/send?phone=59899577533"><img src="{{ asset('/images/whatsapp.png') }}" alt=""></a></section>
    <section id="ft1" style="background-image: url({{ asset('/images/bg1.jpg') }}); background-size: 100%; background-repeat: no-repeat;"></section>
    <footer class="pt-7 pb-5" style="background-color: #aaaaaa; background-size: 100%;">
        <div class="container justify-content-center">
            <div class="row">
                <div class="col-12 col-md-6">
                    <h3 class="mb-4"><img src="{{ asset('/images/footer_button1.png') }}" alt="" style="width: 200px;
    position: relative;
    left: -39px;"></h3>
                    <ul class="list-unstyled text-small">
                        <li class="mb-3">
                            Alberto Mendez 275, Local 013, Colonia del Sacramento, Uruguay.
                        </li>
                        <li class="mb-3">
                            C.P 70000
                        </li>
                        <li class="mb-3">
                            info@blins.com.uy
                        </li>
                        <li class="mb-3">
                            (+598) 99577533
                        </li>
                    </ul>
                    <small class="d-block mb-3">© 2018</small>
                </div>
                <div class="col-6 col-md d-none d-md-block">
                    <h3 class="mb-4"><img src="{{ asset('/images/footer_button2.png') }}" alt="" style="    width: 188px;
    position: relative;
    left: -30px"></h3>
                    <ul class="list-unstyled text-small">
                        <li class="mb-3">
                            <a href="#">
                                Nosotros
                            </a>
                        </li>
                        <li class="mb-3">
                            <a href="#">
                                Tienda
                            </a>
                        </li>
                        <li class="mb-3">
                            <a href="#">
                                Contacto
                            </a>
                        </li>
                        <li class="mb-3">
                            <a href="#">
                                Acceder
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 col-md d-none d-md-block">
                    <h3 class="mb-4"><img src="{{ asset('/images/footer_button3.png') }}" alt="" style="width: 218px; position: relative; left: -10px;"></h3>
                    <ul class="list-unstyled text-small">
                        <li class="mb-3">
                            <a href="https://www.facebook.com/blinsuruguay/" target="_blank">
                                Facebook
                            </a>
                        </li>
                        <li class="mb-3">
                            <a href="https://www.youtube.com/channel/UC9rTbcfBf3gN2XMFF5wqb0A?view_as=subscriber" target="_blank">
                                Youtube
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    @yield('scripts')
    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                        s1.async=true;
                        s1.src='https://embed.tawk.to/5be484a970ff5a5a3a714564/default';
                        s1.charset='UTF-8';
                        s1.setAttribute('crossorigin','*');
                        s0.parentNode.insertBefore(s1,s0);
                    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
