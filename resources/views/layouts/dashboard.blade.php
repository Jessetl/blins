<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="{{ asset('nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/argon.css') }}" rel="stylesheet">
    
</head>
<body>
    <!-- Sidenav -->
            
    @include('layouts.partials.sidenav')

    <!-- Main content -->

    <div class="main-content" id="app">
        
        <!-- Top navbar -->
        
        @include('layouts.partials.navbar')
    
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                   
                </div>
            </div>
        </div>
        
        <!-- Page content -->
        <div class="container-fluid mt--7" id="app">
            
            @yield('content')

            <!-- Footer -->
            <footer class="footer">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left">
                            &copy; 2018 <a href="https://www.efranet.net/" class="font-weight-bold ml-1" target="_blank">Efranet</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>
