@extends('layouts.app')


@section('headerslider')

<div id="slider" class="carousel slide" data-ride="carousel" style="top: 145px;">
    <div class="carousel-inner">
        <div class="carousel-item active">
        <img class="d-block w-100" src="{{ asset('/images/slider.jpg') }}" alt="First slide">
    </div>
</div>

@endsection

@section('content')


<div class="container"> 
	<h1 class="font-weight-light">
		BIENVENIDOS A BLINS
	</h1>
</div>

@endsection
