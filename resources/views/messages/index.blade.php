@extends('layouts.dashboard')

@section('breadcrumbs', 'Sugerencias & Dudas')

@section('content')

<!-- Table -->
<div class="row">
	<div class="col">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Notificaciones</h3>
					</div>
				</div>
			</div>
			<table-messages></table-messages>
		</div>
	</div>
</div>

@endsection
