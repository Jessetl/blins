@extends('layouts.dashboard')

@section('breadcrumbs', 'Postulaciones')

@section('content')

<!-- Table -->
<div class="row">
	<div class="col">
		
		@include('errors.messages')

		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Postulaciones</h3>
					</div>
					<div class="col-4 text-right">
						<a href="{{ route('works.create') }}" class="btn btn-sm btn-primary">NUEVA POSTULACIÓN</a>
					</div>
				</div>
			</div>
			<table-works ></table-works>
		</div>
	</div>
</div>

@endsection
