@extends('layouts.dashboard')

@section('breadcrumbs', 'Categorías')

@section('content')

<!-- Table -->
<div class="row">
	<div class="col">
		
		@include('errors.messages')

		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Categorías</h3>
					</div>
					<div class="col-4 text-right">
						<a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary">NUEVA CATEGORÍA</a>
					</div>
				</div>
			</div>
			<table-categories></table-categories>
		</div>
	</div>
</div>

@endsection
