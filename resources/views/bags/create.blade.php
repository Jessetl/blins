@extends('layouts.app')

@section('content')

<div style="padding-top: 214px;"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-8">
            @include('errors.messages')
        </div>
    </div>
    <div class="row justify-content-center">
        <postulation :bag="{{ $bag }}"></postulation> 
    </div>
</div>

@endsection