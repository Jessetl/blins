@extends('layouts.dashboard')

@section('breadcrumbs', 'Postulación')

@section('content')

<div class="row justify-content-center">
	<div class="col-xl-8 order-xl-1">
		<div class="card bg-secondary shadow">
			<div class="card-header bg-white border-0">
				<div class="row align-items-center">
					<div class="col-md">
						<h3 class="mb-0">{{ $postulation->names }} {{ $postulation->surnames }}</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<embed src="{{ asset($postulation->file) }}#toolbar=0" width="100%" height="700px">
			</div>
		</div>
	</div>
</div>

@endsection