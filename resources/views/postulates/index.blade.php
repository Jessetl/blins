@extends('layouts.dashboard')

@section('breadcrumbs', 'Postulados')

@section('content')

<!-- Table -->
<div class="row">
	<div class="col">
		
		@include('errors.messages')

		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Postulados</h3>
					</div>
				</div>
			</div>
			<postulates></postulates>
		</div>
	</div>
</div>

@endsection
