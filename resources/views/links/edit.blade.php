@extends('layouts.dashboard')

@section('breadcrumbs', 'Editar link')

@section('content')

<div class="row justify-content-center">
	<div class="col-xl-8 order-xl-1">
		<div class="card bg-secondary shadow">
			<div class="card-header bg-white border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Editar enlace</h3>
					</div>
				</div>
			</div>
			<edit-links :link="{{ $link }}" :http="'{{ route('links.update', $link) }}'"></edit-links>
		</div>
	</div>
</div>

@endsection