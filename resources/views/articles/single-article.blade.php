@extends('layouts.app')

@section('content')


<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8 py-5" style="background-image: url({{ asset('/images/login_fondo.jpg') }}); background-size: cover;">
    @if($state == 'Active')
	<div class="row">
		<div class="col-xl-12">
			<img src="{{ asset('/images/titulo_noticias.png') }}" alt="" width="530px">
		</div>
	</div>
	<div class="row">
        <div class="col-md-6">
            <img src="{{ asset('storage') }}/{{ $news->image }}" class="img-fluid" alt="">
        </div>
        <div class="col-md-6">
            <h1>{{ $news->title }}</h1>
            {!! $news->detail !!}
        </div>
    </div>
    @else

    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                Ésta noticia ha finalizado.
            </div>
        </div>
    </div>

    @endif
</div>

@endsection
