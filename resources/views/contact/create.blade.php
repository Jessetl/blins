@extends('layouts.app')

@section('content')

<div style="padding-top: 315px;">
</div>

<div class="container-fluid px-8" style="background-image: url({{ asset('/images/contacto_fondo.jpg') }}); background-size: cover;">
    <div class="row">

        <div class="col-xl-12">
            <img src="{{ asset('/images/titulo_contacto.png') }}" alt="" width="530px">
        </div>

        <div class="col-xl-8 order-xl-1">

            @include('errors.messages')
            
            <div>
                <message></message>
            </div>
        </div>
        <div class="col-xl-4 order-xl-2">
            <div>
                <div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3289.291908120673!2d-57.84722068477386!3d-34.47011838049427!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95a312664b5d892f%3A0x9001d157e72a70ef!2sAlberto+Mendez+275%2C+70000+Col+Del+Sacramento%2C+Departamento+de+Colonia%2C+Uruguay!5e0!3m2!1ses!2sve!4v1515011721704" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
