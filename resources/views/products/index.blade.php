@extends('layouts.dashboard')

@section('breadcrumbs', 'Productos')

@section('content')

<!-- Table -->
<div class="row">
	<div class="col">

		@include('errors.messages')
		
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Productos</h3>
					</div>
					<div class="col-4 text-right">
						<a href="{{ route('products.create') }}" class="btn btn-sm btn-primary">NUEVO PRODUCTO</a>
					</div>
				</div>
			</div>
			<table-products></table-products>
		</div>
	</div>
</div>

@endsection
