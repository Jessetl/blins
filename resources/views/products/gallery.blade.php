@extends('layouts.dashboard')

@section('breadcrumbs', 'Galeria')

@section('content')

<div class="row justify-content-center">
	<div class="col-xl-8 order-xl-1">
		<div class="card bg-secondary shadow">
			<div class="card-header bg-white border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Imagenes</h3>
					</div>
				</div>
			</div>
			<product-gallery :product="{{ $product }}"></product-gallery>
		</div>
	</div>
</div>

@endsection