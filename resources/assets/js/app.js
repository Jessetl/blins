
require('./bootstrap')

window.Vue = require('vue');

Vue.component('categories', require('./components/categories/create.vue'));
Vue.component('edit-categories', require('./components/categories/edit.vue'));
Vue.component('table-categories', require('./components/categories/table.vue'));

Vue.component('links', require('./components/links/create.vue'));
Vue.component('edit-links', require('./components/links/edit.vue'));
Vue.component('table-links', require('./components/links/table.vue'));

Vue.component('videos', require('./components/videos/create.vue'));
Vue.component('edit-videos', require('./components/videos/edit.vue'));
Vue.component('table-videos', require('./components/videos/table.vue'));

Vue.component('users', require('./components/users/create.vue'));
Vue.component('edit-users', require('./components/users/edit.vue'));
Vue.component('table-users', require('./components/users/table.vue'));

Vue.component('news', require('./components/news/create.vue'));
Vue.component('edit-news', require('./components/news/edit.vue'));
Vue.component('table-news', require('./components/news/table.vue'));

Vue.component('works', require('./components/works/create.vue'));
Vue.component('edit-works', require('./components/works/edit.vue'));
Vue.component('table-works', require('./components/works/table.vue'));

Vue.component('products', require('./components/products/create.vue'));
Vue.component('edit-products', require('./components/products/edit.vue'));
Vue.component('table-products', require('./components/products/table.vue'));
Vue.component('product-gallery', require('./components/products/gallery.vue'));

Vue.component('messages', require('./components/messages/create.vue'));
Vue.component('edit-messages', require('./components/messages/edit.vue'));
Vue.component('table-messages', require('./components/messages/table.vue'));

Vue.component('profile', require('./components/profile/edit.vue'));

Vue.component('links-interest', require('./components/links.vue'));
Vue.component('videos-interest', require('./components/videos.vue'));
Vue.component('category-products', require('./components/products.vue'));
Vue.component('message', require('./components/message.vue'));
Vue.component('offers-products', require('./components/offersProducts.vue'));
Vue.component('bags', require('./components/bags.vue'));
Vue.component('articles', require('./components/articles.vue'));
Vue.component('postulation', require('./components/postulation.vue'));
Vue.component('postulates', require('./components/postulates/table.vue'));

const app = new Vue({
    el: '#app'
});

