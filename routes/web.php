<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

## Controllers LandingPage
Route::get('/about-us', 'LandingPageController@about')->name('about');
Route::get('/links-interest', 'LandingPageController@links')->name('links');
Route::get('/videos-interest', 'LandingPageController@videos')->name('videos');

## Controllers Store
Route::get('store/category', 'StoreProductsController@storeCategory')->name('store.category');
Route::get('store/payment_methods', 'StoreProductsController@paymentMethods')->name('payment_methods');
Route::get('store/{category}/products', 'StoreProductsController@store')->name('store');
Route::get('store/{category}', 'StoreProductsController@storeProducts')->name('store.products');
Route::get('store/products/offers', 'StoreProductsController@offers')->name('offers');
Route::get('offers/products', 'StoreProductsController@offersProducts')->name('offers.products');
Route::get('store/product/{product}', 'StoreProductsController@product')->name('store.product');

## Controller News in LandingPage
Route::get('articles', 'ArticlesController@index')->name('articles.index');
Route::get('articles/blins', 'ArticlesController@articles')->name('articles');
Route::get('articles/{article}', 'ArticlesController@singleArticle')->name('articles.single-article');

## Controller LaborBag
Route::get('labor-bag', 'BagsController@index')->name('bags.index');
Route::get('bags', 'BagsController@bags')->name('bags');
Route::get('bags/{work}', 'BagsController@create')->name('bags.create');
Route::post('bags', 'BagsController@store')->name('bags.store');

## Controller Cart
Route::get('cart/show', 'CartController@show')->name('cart.show');
Route::get('cart/add/{product}', 'CartController@add')->name('cart.add');
Route::get('cart/delete/{product}', 'CartController@delete')->name('cart.delete');
Route::get('cart/trash', 'CartController@trash')->name('cart.trash');
Route::get('cart/update/{product}/{quantity}', 'CartController@update')->name('cart.update');

## Controllers Messages
Route::get('message', 'ContactController@create')->name('message.create');
Route::post('message', 'ContactController@store')->name('message.store');

## Controllers Auth Guest
Route::namespace('Auth')->middleware(['web', 'guest'])->group(function () {
	## @Login
	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@login');
	## @Reset Password
	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::post('password/reset', 'ResetPasswordController@reset');
	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
	## @Register
	Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
	Route::post('register', 'RegisterController@register');
	## @Validate
	//Route::get('account/confirm_account/{token_confirmed}', 'ValidateEmailController@validateTokenEmail')->name('confirm_account');
});

Route::namespace('Auth')->middleware(['web'])->group(function () {
	## @Logout
	Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::middleware(['auth', 'web'])->group(function () {
	## @Cart
	Route::get('order-detail', 'CartController@orderDetail')->name('order.detail');

	## @Payment PayPal
	Route::get('payment', 'PaypalController@postPayment')->name('payment');
	Route::get('payment/status', 'PaypalController@getPaymentStatus')->name('payment.status');
});

## Controllers Middleware Auth, Admin
Route::middleware(['auth', 'authPermission'])->group(function () {

	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('feature/{id}/delete', 'ProductsController@destroyFeature')->name('delete.feature');

	Route::get('profile', 'ProfileController@index')->name('profile.index');
	Route::post('profile', 'ProfileController@store')->name('profile.store');

	Route::get('dataTable/categories', 'CategoriesController@dataTable')->name('dataTable.categories');
	Route::get('dataTable/messages', 'MessagesController@dataTable')->name('dataTable.messages');
	Route::get('dataTable/works', 'WorksController@dataTable')->name('dataTable.works');
	Route::get('dataTable/products', 'ProductsController@dataTable')->name('dataTable.products');
	Route::get('dataTable/news', 'NewsController@dataTable')->name('dataTable.news');
	Route::get('dataTable/links', 'LinksController@dataTable')->name('dataTable.links');
	Route::get('dataTable/videos', 'VideosController@dataTable')->name('dataTable.videos');
	Route::get('dataTable/users', 'UsersController@dataTable')->name('dataTable.users');
	Route::get('dataTable/postulates', 'PostulatesController@dataTable')->name('dataTable.postulates');

	Route::get('postulates/{id}/download', 'PostulatesController@download')->name('download.postulates');
	Route::get('products/{product}/gallery', 'ProductsController@gallery')->name('products.gallery');

	Route::post('categories/{category}/update', 'CategoriesController@update')->name('categories.update');
	Route::post('products/{product}/update', 'ProductsController@update')->name('products.update');
	Route::post('works/{work}/update', 'WorksController@update')->name('works.update');

	Route::resource('messages', 'MessagesController');
	Route::resource('works', 'WorksController', ['except' => ['update']]);
	Route::resource('news', 'NewsController');
	Route::resource('users', 'UsersController');
	Route::resource('videos', 'VideosController');
	Route::resource('postulates', 'PostulatesController');
	Route::resource('categories', 'CategoriesController', ['except' => ['update']]);
	Route::resource('products', 'ProductsController', ['except' => ['update']]);
	Route::resource('links', 'LinksController');
});
