<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostulationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $postulation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($postulation)
    {
        $this->postulation = $postulation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                    ->view('emails.postulation')->with('postulation', $this->postulation);
    }
}
