<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    protected $table = 'shoppings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subtotal', 'shipping', 'user_ui'
    ];


    public function user()
    {
    	return $this->belongsTo('App\User', 'user_ui');
    }

    public function product()
    {
    	return $this->belongsToMany('App\Product', 'shopping_items', 'shopping_ui', 'product_ui')->withPivot('quantity', 'price')->withTimestamps();
    }
}
