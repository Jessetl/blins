<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Postulation extends Model
{
    protected $table = 'works_has_requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'work_ui', 'names', 'surnames', 'occupancy', 'email', 'phone', 'file'
    ];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->toFormattedDateString();
    }
    
    public function work()
    {
    	return $this->belongsTo('App\Work', 'work_ui');
    }
}
