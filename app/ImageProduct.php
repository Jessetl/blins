<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageProduct extends Model
{
    protected $table = 'products_has_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_ui', 'image'
    ];

    public function new()
    {
    	return $this->belongsTo('App\Product', 'product_ui');
    }
}
