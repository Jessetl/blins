<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\Video;

class LandingPageController extends Controller
{
    public function about(Request $request)
    {
    	return view('landingpage.about');
    }

    public function links(Request $request)
    {
    	if ( $request->ajax() ) {

    		$links = Link::orderBy('id', 'DESC')->paginate(10);

	        return [
	            'pagination' => [
	                'total'        => $links->total(),
	                'current_page' => $links->currentPage(),
	                'per_page'     => $links->perPage(),
	                'last_page'    => $links->lastPage(),
	                'from'         => $links->firstItem(),
	                'to'           => $links->lastPage(),
	            ],
	            'links' => $links
	        ];
    	}

    	return view('landingpage.links');
    }

    public function videos(Request $request)
    {
    	if ( $request->ajax() ) {

    		$videos = Video::orderBy('id', 'DESC')->paginate(10);

	        return [
	            'pagination' => [
	                'total'        => $videos->total(),
	                'current_page' => $videos->currentPage(),
	                'per_page'     => $videos->perPage(),
	                'last_page'    => $videos->lastPage(),
	                'from'         => $videos->firstItem(),
	                'to'           => $videos->lastPage(),
	            ],
	            'videos' => $videos
	        ];
    	}

    	return view('landingpage.videos');
    }
}
