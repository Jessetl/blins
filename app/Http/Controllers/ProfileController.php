<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class ProfileController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$user = $request->user();
    	
    	$array = [
    		'user' => $user
    	];

    	return view('profile.index', $array);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'names'    => 'required|max:65',
            'surnames' => 'required|max:65',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validatorArray($request->all())->validate();

    	$me = $request->user();
    	$me->names = $request['names'];
    	$me->surnames = $request['surnames'];

    	if ( !empty($request['password']) ) {

    		Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed'
            ])->validate();

            $me->password = Hash::make($request['password']);
    	}

        $me->save();

        $array = [
            'user' => $me
        ];

        $request->session()->flash('success', 'Tu perfil se ha editado.');

    	return response()->json($array);
    }
}
