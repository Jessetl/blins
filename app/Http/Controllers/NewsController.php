<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Article;
use Storage;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('news.index');
    }

    public function dataTable(Request $request)
    {
        $news = Article::with('user')->orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $news->total(),
                'current_page' => $news->currentPage(),
                'per_page'     => $news->perPage(),
                'last_page'    => $news->lastPage(),
                'from'         => $news->firstItem(),
                'to'           => $news->lastPage(),
            ],
            'news' => $news
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|unique:news|max:175',
            'image' => 'required|image',
            'date_ini' => 'required',
            'date_end' => 'required'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();
        $imageName = $request->file('image')->store('news', 'public');
        $new = new Article();
        $new->user_ui = $request->user()->id;
        $new->title = $request['title'];
        $new->slug = $this->replace($request['title']);
        $new->detail = $request['detail'];
        $new->date_ini = $request['date_ini'];
        $new->date_end = $request['date_end'];
        $new->image = $imageName;
        $new->save();

        return redirect('/news')->with(session()->flash('success', 'La noticia ' . $new->title . ' ha sido agregada.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::with('images')->whereSlug($id)->firstOrFail();

        return view('news.edit')->with(['article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function update(NewsUpdateRequest $request, $id)
    {
        $new = Article::with('images')->whereSlug($id)->firstOrFail();
        $imageName = $request->file('image')->store('news', 'public');
        $new->title = $request['title'];
        $new->slug = $this->replace($request['title']);
        $new->detail = $request['detail'];
        $new->date_ini = $request['date_ini'];
        $new->date_end = $request['date_end'];
        $new->image = $imageName;
        $new->save();

        return redirect('/news')->with(session()->flash('success', 'La noticia ' . $new->title . ' ha sido editada.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = Article::whereSlug($id)->firstOrFail();
        $new->delete();

        $request->session()->flash('success', 'La noticia ' . $new->title . ' ha sido borrada.');

        return response()->json($array);
    }
}
