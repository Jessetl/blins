<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Message;
use Session;

class ContactController extends Controller
{
    public function create(Request $request)
    {
    	return view('contact.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'title'      => 'required|max:175',
            'email'      => 'required|email',
            'names'      => 'required|max:65',
            'surnames'   => 'required|max:65',
            'phone'      => 'required|max:35',
            'detail'     => 'required'
        ]);
    }

    public function store(Request $request)
    {
    	$this->validatorArray($request->all())->validate();

    	$message = new Message();
    	$message->user_ui = empty($request->user()) ? null : $request->user()->id;
    	$message->title = $request['title'];
    	$message->slug = $this->replace($request['title']);
    	$message->email = $request['email'];
    	$message->names = $request['names'];
    	$message->surnames = $request['surnames'];
    	$message->phone = $request['phone'];
    	$message->detail = $request['detail'];
    	$message->save();

    	$array = [
    		'message' => $message
    	];

        $request->session()->flash('success', 'Tu correo ha sido enviado, gracias por contactar con nosotros.');

    	return response()->json($array);
    }
}
