<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Link;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('links.index');
    }

    public function dataTable(Request $request)
    {
        $links = Link::orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $links->total(),
                'current_page' => $links->currentPage(),
                'per_page'     => $links->perPage(),
                'last_page'    => $links->lastPage(),
                'from'         => $links->firstItem(),
                'to'           => $links->lastPage(),
            ],
            'links' => $links
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('links.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'title'  => 'required|unique:links|max:175',
            'uri'    => 'required|max:190'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $link = new Link();
        $link->title = $request['title'];
        $link->slug = $this->replace($request['title']);
        $link->detail = $request['detail'];
        $link->uri = $request['uri'];
        $link->save();

        $array = [
            'link' => $link
        ];

        $request->session()->flash('success', 'El enlace ' . $link->title . ' ha sido agregado.');

        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        $array = [
            'link' => $link
        ];
        
        return view('links.edit', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {
        Validator::make($request->all(), [
            'uri' => 'required',
            'title' => [
                'required',
                Rule::unique('links')->ignore($link->id),
            ],
        ])->validate();

        $link->title = $request['title'];
        $link->slug = $this->replace($request['title']);
        $link->detail = $request['detail'];
        $link->uri = $request['uri'];
        $link->save();

        $array = [
            'link' => $link
        ];

        $request->session()->flash('success', 'El enlace ' . $link->title . ' ha sido editado.');

        return response()->json($array); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Link $link)
    {
        $link->delete();

        $request->session()->flash('success', 'El enlace ' . $link->title . ' ha sido borrado.');

        return response()->json($array);
    }
}
