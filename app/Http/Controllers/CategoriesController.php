<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Category;
use Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categories.index');
    }

    public function dataTable(Request $request)
    {
        $categories = Category::orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $categories->total(),
                'current_page' => $categories->currentPage(),
                'per_page'     => $categories->perPage(),
                'last_page'    => $categories->lastPage(),
                'from'         => $categories->firstItem(),
                'to'           => $categories->lastPage(),
            ],
            'categories' => $categories
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|unique:categories|max:65'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $category = new Category();
        $category->name = $request['name'];
        $category->slug = $this->replace($request['name']);
        $category->detail = $request['detail'];

        if ( !empty($request->file('file')) ) {
            $fileName = $request->file('file')->store('category', 'public');
            $category->image = $fileName;
        }

        $category->save();

        $array = [
            'category' => $category
        ];
        
        $request->session()->flash('success', 'La categoría ' . $category->name . ' ha sido agregada.');

        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $array = [
            'category' => $category
        ];

        return view('categories.edit', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('categories')->ignore($category->id),
            ],
        ])->validate();

        $category->name = $request['name'];
        $category->slug = $this->replace($request['name']);
        $category->detail = $request['detail'];

        if ( !empty($request->file('file')) ) {
            
            if ( !empty($category->image) ) \File::Delete($this->uri().'/'.$category->image);

            $fileName = $request->file('file')->store('category', 'public');
            $category->image = $fileName;

        }

        $category->save();

        $array = [
            'category' => $category
        ];

        $request->session()->flash('success', 'La categoría ' . $category->name . ' ha sido editada.');

        return response()->json($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        $category->delete();

        $request->session()->flash('success', 'La categoría ' . $category->name . ' ha sido borrada.');

        return response()->json();
    }

    protected function uri()
    {
        return public_path().'/storage';
    }
}
