<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use App\Shopping;

class PaypalController extends Controller
{
	private $apiContext;
    private $mode;
    private $client_id;
    private $secret;

	public function __construct()
	{
		// Detect if we are running in live mode or sandbox
        if (config('paypal.settings.mode') == 'live') {

            $this->client_id = config('paypal.live_client_id');
            $this->secret = config('paypal.live_secret');

        } else {

            $this->client_id = config('paypal.sandbox_client_id');
            $this->secret = config('paypal.sandbox_secret');

        }
        
        // Set the Paypal API Context/Credentials
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));
	}

    public function postPayment()
    {
    	$payer = new Payer();
    	$payer->setPaymentMethod('paypal');

    	$items = array();
    	$subtotal = 0;
    	$cart = \Session::get('cart');

    	$currency = 'USD';

    	foreach ($cart as $key => $producto) {

    		$producto->price = $producto->offer ? $producto->feeoffer : $producto->fee;

    		$item = new Item();
    		$item->setName($producto->name)
    		->setCurrency($currency)
    		->setDescription($producto->detail)
    		->setQuantity($producto->quantity)
    		->setPrice($producto->price);

    		$items[] = $item;
    		$subtotal += $producto->quantity * $producto->price;
    	}

    	$item_list = new ItemList();
    	$item_list->setItems($items);

    	$details = new Details();
    	$details->setSubTotal($subtotal)
    	->setShipping(100);

    	$total = $subtotal + 100;

    	$amount = new Amount();
    	$amount->setCurrency($currency)
    	->setTotal($total)
    	->setDetails($details);

    	$transaction = new Transaction();
    	$transaction->setAmount($amount)
    	->setItemList($item_list)
    	->setDescription('Pedido de prueba en mi Laravel App Store');

    	$redirect_urls = new RedirectUrls();
		$redirect_urls->setReturnUrl(\URL::route('payment.status'))
			->setCancelUrl(\URL::route('payment.status'));

		$payment = new Payment();
		$payment->setIntent('Sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));

		try {

			$payment->create($this->apiContext);

		} catch (\PayPal\Exception\PPConnectionException $ex) {

			if (\Config::get('app.debug')) {

				echo "Exception: " . $ex->getMessage() . PHP_EOL;
				$err_data = json_decode($ex->getData(), true);
				exit;

			} else {

				die('Ups! Algo salió mal');
			}
		}

		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}

		\Session::put('paypal_payment_id', $payment->getId());
		
		if(isset($redirect_url)) {
			// redirect to paypal
			return \Redirect::away($redirect_url);
		}

		return \Redirect::route('cart.show')
			->with('error', 'Ups! Error desconocido.');
    }

    public function getPaymentStatus(Request $request)
    {
    	// Get the payment ID before session clear
		$payment_id = \Session::get('paypal_payment_id');

		// clear the session payment ID
		\Session::forget('paypal_payment_id');

		$payerId = $request['PayerID'];
        $token = $request['token'];

		//if (empty(\Input::get('PayerID')) || empty(\Input::get('token'))) {
		if (empty($payerId) || empty($token)) {
			return \Redirect::route('order.detail')
				->with('error', 'Hubo un problema al intentar pagar con Paypal');
		}

		$payment = Payment::get($payment_id, $this->apiContext);

		$execution = new PaymentExecution();
        $execution->setPayerId($payerId);
     	
     	$result = $payment->execute($execution, $this->apiContext);

     	if ($result->getState() == 'approved') {

     		$this->saveShopping(\Session::get('cart'));

            \Session::forget('cart');

            return \Redirect::route('cart.show')
            	->with('success', 'Compra realizada de forma correcta');
        }

        return redirect('order-detail')->with('error', 'Paypal ha cancelado esta compra, intentelo mas tarde');
    }

    private function saveShopping($cart)
	{
	    $subtotal = 0;	   

	    foreach ($cart as $key => $item) {
	    	if ( $item->offer ) {
	        	$subtotal += $item->feeoffer * $item->quantity;
	    	} else {
	    		$subtotal += $item->fee * $item->quantity;
	    	}
	    }
	    
	    $shopping = Shopping::create([
	        'subtotal' => $subtotal,
	        'shipping' => 100,
	        'user_ui' => \Auth::user()->id
	    ]);
	    
	    foreach ($cart as $key => $item) {

	        $this->saveShoppingItem($item, $shopping);
	    }
	}

	private function saveShoppingItem($item, $shopping)
	{	
		$item->price = $item->offer ? $item->feeoffer : $item->fee;

		$shopping->product()->attach($shopping->id, ['quantity' => $item->quantity, 'price' => $item->price]);
	}
}
