<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    public function dataTable(Request $request)
    {
        $users = User::orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $users->total(),
                'current_page' => $users->currentPage(),
                'per_page'     => $users->perPage(),
                'last_page'    => $users->lastPage(),
                'from'         => $users->firstItem(),
                'to'           => $users->lastPage(),
            ],
            'users' => $users
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'names' => 'required|string|max:65',
            'surnames' => 'required|string|max:65',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $user = new User();
        $user->names = $request['names'];
        $user->surnames = $request['surnames'];
        $user->email = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->token_confirmed = $request['token_confirmed'];
        $user->type = $request['type'];
        $user->save();

        $array = [
            'user' => $user
        ];

        $request->session()->flash('success', 'El usuario ' . $user->names . ' ha sido agregado.');

        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $array = [
            'user' => $user
        ];

        return view('users.edit', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        Validator::make($request->all(), [
            'names' => 'required|string|max:65',
            'surnames' => 'required|string|max:65',
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ],
        ])->validate();

        $user->names = $request['names'];
        $user->surnames = $request['surnames'];
        $user->email = $request['email'];
        
        if ( !empty($request['password']) ) {

            Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed'
            ])->validate();

            $user->password = Hash::make($request['password']);
        }

        $user->token_confirmed = $request['token_confirmed'];
        $user->type = $request['type'];
        $user->save();

        $array = [
            'user' => $user
        ];

        $request->session()->flash('success', 'El usuario ' . $user->names . ' ha sido editado.');

        return response()->json($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        $array = [
            'success' => 'Usuario ' . $user->names . ' borrado satisfactoriamente.'
        ];

        return response()->json($array);
    }
}
