<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Work;
use App\Postulation;
use Carbon\Carbon;
use Storage;

use App\Mail\PostulationEmail;
use Illuminate\Support\Facades\Mail;

class BagsController extends Controller
{
    public function index(Request $request)
    {
    	return view('bags.index');
    }

    public function bags(Request $request)
    {
    	$bags = Work::orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $bags->total(),
                'current_page' => $bags->currentPage(),
                'per_page'     => $bags->perPage(),
                'last_page'    => $bags->lastPage(),
                'from'         => $bags->firstItem(),
                'to'           => $bags->lastPage(),
            ],
            'bags' => $bags
        ];
    }

    public function create(Work $work)
    {
        $array = [
            'bag' => $work
        ];

        return view('bags.create', $array);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'names'      => 'required|max:75',
            'surnames'   => 'required|max:75',
            'occupancy'  => 'required|max:145',
            'email'      => 'required|max:65',
            'phone'      => 'required|max:16'
        ]);
    }

    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $fileName = $request->file('file')->store('files', 'public');

        $postulation = new Postulation();
        $postulation->work_ui = $request['bag'];
        $postulation->names = $request['names'];
        $postulation->surnames = $request['surnames'];
        $postulation->occupancy = $request['occupancy'];
        $postulation->email = $request['email'];
        $postulation->phone = $request['phone'];
        $postulation->file = $fileName;
        $postulation->save();

        Mail::to(['rrhh@blins.com.uy','danieljtorres94@gmail.com'])->send(new PostulationEmail($postulation));

        $array = [
            'postulation' => $postulation
        ];

        $request->session()->flash('success', 'Tu postulación ha sido enviada.');
        
        return response()->json($array);
    }

    protected function fileName($name)
    {
        return Carbon::now()->toDateString();
    }
}
