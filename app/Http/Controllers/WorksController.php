<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Work;

class WorksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('works.index');
    }

    public function dataTable(Request $request)
    {
        $works = Work::orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $works->total(),
                'current_page' => $works->currentPage(),
                'per_page'     => $works->perPage(),
                'last_page'    => $works->lastPage(),
                'from'         => $works->firstItem(),
                'to'           => $works->lastPage(),
            ],
            'works' => $works
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('works.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'title'      => 'required|unique:works|max:175',
            'area'       => 'required|max:125',
            'occupancy'  => 'required|max:145',
            'experience' => 'required|max:65',
            'detail'     => 'required',
            'salary'     => 'required|max:175',
            'contact'    => 'required|max:190',
            'work_end'   => 'required'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $work = new Work();
        $work->title = $request['title'];
        $work->slug = $this->replace($request['title']);
        $work->area = $request['area'];
        $work->occupancy = $request['occupancy'];
        $work->experience = $request['experience'];
        $work->detail = $request['detail'];
        $work->salary = $request['salary'];
        $work->contact = $request['contact'];
        $work->work_end = $request['work_end'];

        if ( !empty($request->file('file')) ) {
            $fileName = $request->file('file')->store('works', 'public');
            $work->image = $fileName;
        }

        $work->save();

        $array = [
            'work' => $work
        ];  

        $request->session()->flash('success', 'La postulación ' . $work->title . ' ha sido agregada.');

        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work)
    {
        $array = [
            'work' => $work
        ];

        return view('works.edit', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
        Validator::make($request->all(), [
            'title' => [
                'required',
                Rule::unique('works')->ignore($work->id),
            ],
            'area'       => 'required|max:125',
            'occupancy'  => 'required|max:145',
            'experience' => 'required|max:65',
            'detail'     => 'required',
            'salary'     => 'required|max:175',
            'contact'    => 'required|max:190',
            'work_end'   => 'required'
        ])->validate();

        $work->title = $request['title'];
        $work->slug = $this->replace($request['title']);
        $work->area = $request['area'];
        $work->occupancy = $request['occupancy'];
        $work->experience = $request['experience'];
        $work->detail = $request['detail'];
        $work->salary = $request['salary'];
        $work->contact = $request['contact'];
        $work->work_end = $request['work_end'];

        if ( !empty($request->file('file')) ) {
            
            if ( !empty($work->image) ) \File::Delete($this->uri().'/'.$work->image);

            $fileName = $request->file('file')->store('works', 'public');
            $work->image = $fileName;
        }

        $work->save();

        $array = [
            'work' => $work
        ];

        $request->session()->flash('success', 'La postulación ' . $work->title . ' ha sido editada.');

        return response()->json($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
        $work->delete();

        $array = [
            'success' => 'Postulación ' . $work->title . ' borrada satisfactoriamente...'
        ];

        $request->session()->flash('success', 'La postulación ' . $work->title . ' ha sido borrada.');

        return response()->json($array);
    }

    protected function uri()
    {
        return public_path().'/storage';
    }
}
