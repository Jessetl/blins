<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Product;
use App\Category;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index');
    }

    public function dataTable(Request $request)
    {
        $products = Product::with('category')->orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page'     => $products->perPage(),
                'last_page'    => $products->lastPage(),
                'from'         => $products->firstItem(),
                'to'           => $products->lastPage(),
            ],
            'products' => $products
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->getCategories();

        $array = [
            'categories' => $categories
        ];

        return view('products.create', $array);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'name'       => 'required|unique:products|max:175',
            'fee'        => 'required|min:1',
            'feeresale'  => 'required|numeric|min:1',
            'category'   => 'required'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $product = new Product();
        $product->name = $request['name'];
        $product->slug = $this->replace($request['name']);

        if (is_numeric($request['fee'])) {
            $product->fee = $request['fee'];
        } else {
            $product->fee = 0;
            $product->fee_alpha = $request['fee'];
        }
        $product->feeresale = $request['feeresale'];
        $product->category_ui = $request['category'];
        $product->detail = $request['detail'];

        if ( !empty($request['feeoffer']) ) {

            Validator::make($request->all(), [
                'feeoffer'    => 'required',
                'offer_begin' => 'required',
                'offer_end'   => 'required'
            ])->validate();

            $product->offer = 1;
            $product->feeoffer = $request['feeoffer'];
            $product->offer_begin = $request['offer_begin'];
            $product->offer_end = $request['offer_end'];
        }

        if ( !empty($request->file('file'))) {
            $fileName = $request->file('file')->store('products', 'public');
            $product->image = $fileName;
        }

        $product->save();

        if ($request['features'] && count($request['features']) > 0) {
            foreach ($request['features'] as $key => $feature) {
            $feature = new \App\Feature(['detail' => $feature]);
            $product->features()->save($feature);
        }

        }

        $array = [
            'product' => $product
        ];
        
        $request->session()->flash('success', 'El producto ' . $product->name . ' ha sido agregado.');

        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = $this->getCategories();

        $product->load('features', 'category');

        $array = [
            'product' => $product,
            'categories' => $categories
        ];

        return view('products.edit', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('products')->ignore($product->id),
            ],
            'fee'        => 'required|numeric|min:1',
            'feeresale'  => 'required|numeric|min:1',
            'category'   => 'required'
        ])->validate();

        $product->name = $request['name'];
        $product->slug = $this->replace($request['name']);
        $product->fee = $request['fee'];
        $product->feeresale = $request['feeresale'];
        $product->category_ui = $request['category'];
        $product->detail = $request['detail'];

        if ( !empty($request['feeoffer']) ) {

            Validator::make($request->all(), [
                'feeoffer'    => 'required',
                'offer_begin' => 'required',
                'offer_end'   => 'required'
            ])->validate();

            $product->offer = 1;
            $product->feeoffer = $request['feeoffer'];
            $product->offer_begin = $request['offer_begin'];
            $product->offer_end = $request['offer_end'];
        }

        if ( !empty($request->file('file')) ) {
            
            if ( !empty($product->image) ) \File::Delete($this->uri().'/'.$product->image);

            $fileName = $request->file('file')->store('products', 'public');
            $product->image = $fileName;

        }

        $product->save();

        if ( !empty($request['features']) ) {
            foreach ($request['features'] as $key => $feature) {
                
                $feature = new \App\Feature(['detail' => $feature]);
                $product->features()->save($feature);
            }
        }

        $array = [
            'product' => $product
        ];
        
        $request->session()->flash('success', 'El producto ' . $product->name . ' ha sido editado.');

        return response()->json($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        $product->delete();

        $request->session()->flash('success', 'El producto ' . $product->name . ' ha sido borrado.');
        
        return response()->json($array);
    }

    public function gallery(Product $product)
    {
        $product->load('images');

        $array = [ 
            'product' => $product
        ];

        return view('products.gallery', $array);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFeature($id)
    {
        $feature = \App\Feature::findOrFail($id);
        $feature->delete();

        return response()->json();
    }

    protected function uri()
    {
        return public_path().'/storage';
    }
}

