<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Session;
use DB;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
    	return view('articles.index');
    }

    public function articles(Request $request)
    {
    	$articles = Article::with('user')->orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $articles->total(),
                'current_page' => $articles->currentPage(),
                'per_page'     => $articles->perPage(),
                'last_page'    => $articles->lastPage(),
                'from'         => $articles->firstItem(),
                'to'           => $articles->lastPage(),
            ],
            'articles' => $articles
        ];
    }

    public function singleArticle($article)
    {

        $news = DB::table('news')->where('slug', $article)->first();
        $state = 'Active';

        if ($news) {
            $date_ini = $news->date_ini;
            $date_end = $news->date_end; // Fecha que finaliza la noticia.

            if (strtotime(date('Y-m-d')) >= strtotime($date_end)) { // Si fecha final es mayor al día de hoy.
                $state = 'Inactive';
            }
        }
        
        
        return view('articles.single-article')->with(['news' => $news, 'state' => $state]);
    }
}
