<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work;
use App\Postulation;
use Storage;

class PostulatesController extends Controller
{
    public function index(Request $request)
    {
    	return view('postulates.index');
    }

    public function dataTable(Request $request)
    {
    	$postulates = Postulation::orderBy('created_at', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $postulates->total(),
                'current_page' => $postulates->currentPage(),
                'per_page'     => $postulates->perPage(),
                'last_page'    => $postulates->lastPage(),
                'from'         => $postulates->firstItem(),
                'to'           => $postulates->lastPage(),
            ],
            'postulates' => $postulates
        ];
    }

    public function download($id)
    {
        $postulate = Postulation::findOrFail($id);

        $path = public_path()."/storage/$postulate->file";

        return response()->download($path);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $postulate = Postulation::findOrFail($id);
        $postulate->delete();

        $request->session()->flash('success', 'La postulación de ' . $postulate->names . ' ha sido borrada.');

        return response()->json();
    }
}
