<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Video;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('videos.index');
    }

    public function dataTable(Request $request)
    {
        $videos = Video::orderBy('id', 'DESC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $videos->total(),
                'current_page' => $videos->currentPage(),
                'per_page'     => $videos->perPage(),
                'last_page'    => $videos->lastPage(),
                'from'         => $videos->firstItem(),
                'to'           => $videos->lastPage(),
            ],
            'videos' => $videos
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('videos.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorArray(array $data)
    {
        return Validator::make($data, [
            'title'  => 'required|unique:links|max:175',
            'uri'    => 'required|max:190'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatorArray($request->all())->validate();

        $video = new Video();
        $video->title = $request['title'];
        $video->slug = $this->replace($request['title']);
        $video->detail = $request['detail'];
        $video->uri = $request['uri'];
        $video->save();

        $array = [
            'video' => $video
        ];

        $request->session()->flash('success', 'El video ' . $video->title . ' ha sido agregado.');

        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        $array = [
            'video' => $video
        ];

        return view('videos.edit', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        Validator::make($request->all(), [
            'title' => [
                'required',
                Rule::unique('videos')->ignore($video->id),
            ],
        ])->validate();

        $video->title = $request['title'];
        $video->slug = $this->replace($request['title']);
        $video->detail = $request['detail'];
        $video->uri = $request['uri'];
        $video->save();

        $array = [
            'video' => $video
        ];

        $request->session()->flash('success', 'El video ' . $video->title . ' ha sido editado.');

        return response()->json($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Video $video)
    {
        $video->delete();

        $request->session()->flash('success', 'El video ' . $video->title . ' ha sido borrado.');

        return response()->json($array);
    }
}
