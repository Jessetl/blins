<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use DB;

class StoreProductsController extends Controller
{
	public function store(Category $category)
	{
        $array = [
            'category' => $category
        ];

        return view('store.products', $array);
	}

    public function storeProducts(Request $request)
    {
        $products = Product::with('category')->where('category_ui', $request['category'])->orderBy('name', 'ASC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page'     => $products->perPage(),
                'last_page'    => $products->lastPage(),
                'from'         => $products->firstItem(),
                'to'           => $products->lastPage(),
            ],
            'products' => $products
        ];
    }

    public function storeCategory(Request $request)
    {
    	$categories = Category::orderBy('name', 'ASC')->get();

    	$array = [
    		'categories' => $categories
    	];

    	return view('store.category', $array);
   	}

    public function offers(Request $request)
    { 
        return view('store.offers');
    }

    public function offersProducts(Request $request)
    {
        $products = Product::with('category')->where('offer', true)->orderBy('name', 'ASC')->paginate(10);

        return [
            'pagination' => [
                'total'        => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page'     => $products->perPage(),
                'last_page'    => $products->lastPage(),
                'from'         => $products->firstItem(),
                'to'           => $products->lastPage(),
            ],
            'products' => $products
        ];
    }

    public function product(Product $product)
    {
        $product->load(['features', 'category', 'images']);

        $array = [
            'product' => $product
        ];

        return view('store.product', $array);
    }

    public function paymentMethods()
    {

        $payments_methods = DB::table('payment_methods')->get();

        return view('store.payment_methods', compact('payments_methods'));
    }
}
