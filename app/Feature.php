<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'products_has_features';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_ui', 'detail'
    ];

    public function product()
    {
    	return $this->belongsTo('App\Product', 'product_ui');
    }
}
