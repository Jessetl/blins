<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'name', 'slug', 'detail'
    ];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products() 
    {
        return $this->hasMany('App\Product', 'category_ui', 'id');
    }
}
