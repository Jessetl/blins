<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'detail', 'uri'
    ];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
