<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_ui', 'image', 'title', 'slug', 'detail'
    ];
    
    public function getRouteKeyName()
    {
    	return 'slug';
    }    

    public function user()
    {
        return $this->belongsTo('App\User', 'user_ui');
    }

    public function images()
    {
        return $this->hasMany('App\NewImage', 'new_ui');
    }
}
