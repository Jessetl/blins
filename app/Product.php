<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'fee', 'feeresale', 'category_ui', 'detail', 'image', 'offer', 'offer_begin', 'offer_end', 'feeoffer'
    ];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_ui');
    }

    public function features()
    {
        return $this->hasMany('App\Feature', 'product_ui', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\ImageProduct', 'product_ui', 'id');
    }

    public function shopping()
    {
        return $this->belongsToMany('App\Shopping', 'shopping_items', 'product_ui', 'shopping_ui')->withPivot('quantity', 'price')->withTimestamps();
    }
}
