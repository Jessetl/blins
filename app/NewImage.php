<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewImage extends Model
{
    protected $table = 'news_has_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'new_ui', 'image'
    ];

    public function new()
    {
    	return $this->belongsTo('App\Article', 'new_ui');
    }
}
