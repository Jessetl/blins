<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
	protected $table = 'works';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'area', 'occupancy', 'experience', 'detail', 'salary', 'contact', 'work_end'
    ];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function postulations()
    {
        return $this->hasMany('App\Postulation', 'work_ui', 'id');
    }
}
