-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-10-2018 a las 11:18:54
-- Versión del servidor: 10.2.18-MariaDB-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blinscom_database`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(115) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `image`, `name`, `slug`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'tipo_CCTV.jpeg', 'CCTV', 'cctv', NULL, '2018-10-24 01:58:03', '2018-10-24 01:58:03'),
(2, 'tipo_ALARMAS.jpeg', 'Alarmas', 'alarmas', NULL, '2018-10-24 01:58:12', '2018-10-24 01:58:12'),
(3, 'tipo_SMARTPANICS.jpeg', 'Smartpanics', 'smartpanics', NULL, '2018-10-24 01:58:20', '2018-10-24 01:58:20'),
(4, 'tipo_SUMINSTRO_DE_PERSONAL.jpeg', 'Suministro de Personal', 'suministro-de-personal', NULL, '2018-10-24 01:58:28', '2018-10-24 01:58:28'),
(5, 'tipo_CENTRAL_DE_MONITOREO.jpeg', 'Central de Monitoreo', 'central-de-monitoreo', NULL, '2018-10-24 01:58:34', '2018-10-24 01:58:34'),
(6, 'tipo_VIDEOVERIFICACION.jpeg', 'Videoverificación', 'videoverificaci-n', NULL, '2018-10-24 01:58:50', '2018-10-24 01:58:50'),
(7, 'tipo_VIDEOVIGILANCIA.jpeg', 'Videovigilancia', 'videovigilancia-46897', NULL, '2018-10-24 01:58:58', '2018-10-25 21:28:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uri` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `links`
--

INSERT INTO `links` (`id`, `title`, `slug`, `detail`, `uri`, `created_at`, `updated_at`) VALUES
(1, 'Foro de Profesionales Latinoamericanos de Seguridad', 'foro-de-profesionales-latinoamericanos-de-seguridad', NULL, 'http://forodeseguridad.com/', '2018-10-17 23:57:04', '2018-10-17 23:57:04'),
(2, 'Diario de Colonia del Sacramento', 'diario-de-colonia-del-sacramento', NULL, 'http://coloniaya.com/', '2018-10-17 23:57:17', '2018-10-17 23:57:17'),
(3, 'Noticias de Uruguay', 'noticias-de-uruguay', NULL, 'http://www.elpais.com.uy/', '2018-10-17 23:57:32', '2018-10-17 23:57:32'),
(4, 'Info Bae America', 'info-bae-america', NULL, 'http://www.Infobae.com/', '2018-10-17 23:57:45', '2018-10-17 23:57:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_ui` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(95) COLLATE utf8mb4_unicode_ci NOT NULL,
  `names` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surnames` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `messages`
--

INSERT INTO `messages` (`id`, `user_ui`, `title`, `slug`, `email`, `names`, `surnames`, `phone`, `detail`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Pruebas', '', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '04124968557', 'Esto es un mensaje de prueba', '2018-10-24 18:59:41', '2018-10-24 18:59:41'),
(4, NULL, 'Pruebas de sment', 'pruebas-de-sment', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '04124968557', 'Esto es un mensaje de prueba', '2018-10-24 19:01:09', '2018-10-24 19:01:09'),
(5, NULL, 'Secret', 'secret', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '04124968857', 'Detalles', '2018-10-24 19:14:29', '2018-10-24 19:14:29'),
(6, 1, 'Secreto', 'secreto', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '04124968857', 'Detalles', '2018-10-24 19:15:37', '2018-10-24 19:15:37'),
(7, NULL, 'fsdfdsf', 'fsdfdsf', 'example@gmail.com', 'Jesus', 'Matute', '04124968557', 'Number one', '2018-10-25 17:46:45', '2018-10-25 17:46:45'),
(8, NULL, 'Hola de nuevo', 'hola-de-nuevo', 'example@gmail.com', 'Jesus', 'Matute', '0412-496855777777777777777777777777', 'ninguna', '2018-10-25 17:54:31', '2018-10-25 17:54:31'),
(9, NULL, 'jejejeje', 'jejejeje', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '04124968557', 'prueba efranet', '2018-10-25 17:55:33', '2018-10-25 17:55:33'),
(10, NULL, 'hola', 'hola-5', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '123456789', 'prueba numero 2', '2018-10-25 18:00:30', '2018-10-25 18:00:30'),
(11, NULL, 'Pruebas de sment', 'pruebas-de-sment-99404', 'mtr_1101@hotmail.com', 'Jesus', 'Matute', '12345678', 'Prueba sment', '2018-10-25 18:01:36', '2018-10-25 18:01:36'),
(12, NULL, 'Hola', 'hola-24786', 'info@blins.com.uy', 'Javier', 'Garcia', '099577533', 'Prueba', '2018-10-26 05:06:47', '2018-10-26 05:06:47'),
(13, NULL, 'Busco trabajo', 'busco-trabajo-84993', 'bettyquintana32@gmail.com', 'Betty Beronica', 'Quintana González', '092989816', 'Tengo experiencia en hoteleria, trabajé 2 años de mucama en sheraton, eh trabajado en restaurantes de bachera y también resepcionista, con muy bueno predisposición', '2018-10-27 07:41:41', '2018-10-27 07:41:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(40, '2014_10_12_000000_create_users_table', 1),
(41, '2014_10_12_100000_create_password_resets_table', 1),
(42, '2018_10_03_144753_create_works_table', 1),
(43, '2018_10_03_145153_create_links_table', 1),
(44, '2018_10_03_145314_create_news_table', 1),
(45, '2018_10_03_145358_create_categories_table', 1),
(46, '2018_10_03_150303_create_videos_table', 1),
(47, '2018_10_03_150306_create_products_table', 1),
(48, '2018_10_03_151141_create_messages_table', 1),
(49, '2018_10_03_151219_create_works_has_requests_table', 1),
(50, '2018_10_03_151304_create_news_has_images_table', 1),
(51, '2018_10_03_151448_create_products_has_features_table', 1),
(52, '2018_10_03_151503_create_products_has_images_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_ui` int(10) UNSIGNED NOT NULL,
  `image` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`id`, `user_ui`, `image`, `title`, `slug`, `detail`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Bienvenidos a blins!', 'bienvenidos-a-blins--36344', 'BLINS es uno de los centros de operaciones de la RED NACIONAL DE SEGURIDAD con proyección a nivel nacional. Nuestro anhelo es estar cada vez más cerca de nuestros Clientes, ofreciendo una gama de soluciones integrales a un mercado exigente y altamente sofisticado.', '2018-10-25 21:54:47', '2018-10-25 21:54:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news_has_images`
--

CREATE TABLE `news_has_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `new_ui` int(10) UNSIGNED NOT NULL,
  `image` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee` int(11) NOT NULL,
  `feeresale` int(11) NOT NULL,
  `category_ui` int(10) UNSIGNED NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(115) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer` tinyint(1) NOT NULL DEFAULT 0,
  `offer_begin` date DEFAULT NULL,
  `offer_end` date DEFAULT NULL,
  `feeoffer` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `fee`, `feeresale`, `category_ui`, `detail`, `image`, `offer`, `offer_begin`, `offer_end`, `feeoffer`, `created_at`, `updated_at`) VALUES
(1, 'Kit Básico Alarma DSC 585 Profesional Cableado', 'kit-b-sico-alarma-dsc-585-profesional-cableado', 249, 75, 2, 'Panel, Teclado, Gabinete, Trafo, Batería, 2 Sensores de Movimiento I/R', 'pro_5KIT_BASICO_ALARMA_DSC.jpg', 1, '2018-01-01', '2018-11-30', 199, '2018-10-24 02:51:48', '2018-10-24 02:51:48'),
(2, 'Kit Básico Alarma Inalámbrica 2018 E (Opción de Control por App)', 'kit-b-sico-alarma-inal-mbrica-2018-e-opci-n-de-control-por-app-', 325, 100, 2, 'Panel, Teclado, Gabinete, Trafo, Batería, 1 Sirena, 2 Receptor, 2 Sensores y 1 Magnético (1 Llavero de Cortesía)', 'pro_4ALARMAS_INTELBRAS_I.jpeg', 1, '2018-01-01', '2018-11-30', 249, '2018-10-24 02:53:31', '2018-10-24 02:53:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_has_features`
--

CREATE TABLE `products_has_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_ui` int(10) UNSIGNED NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_has_images`
--

CREATE TABLE `products_has_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_ui` int(10) UNSIGNED NOT NULL,
  `image` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(175) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `names` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surnames` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(175) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `type` enum('root','administrator','customer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `url`, `names`, `surnames`, `email`, `password`, `token`, `token_confirmed`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Jesus Eduardo', 'Matute Rangel', 'mtr_1101@hotmail.com', '$2y$10$56AFZNxabeZDUZqbEs4UE.ZDAnDS/O2IvGudCYU6tI7TgGbWX/ORe', NULL, 0, 'customer', 'P3K1JvDDmlIyA9yYuX7FzJfiN9NQBeTPZMtWzk10yjYgK6r6FlZNBJIn25g5', '2018-10-17 23:54:27', '2018-10-19 03:02:55'),
(2, NULL, 'Evelyeth Roxana', 'Mora Chacon', 'evelyeth.mora@gmail.com', '$2y$10$1rv5GgnF7ARu7e.PVZ1MauUcbY/62gg2P7vsAH0GfLiPD9BOkwvHq', NULL, 0, 'customer', NULL, '2018-10-27 01:27:53', '2018-10-27 01:27:53'),
(3, NULL, 'Fabian', 'Roqueta', 'roqueta71@gmail.com', '$2y$10$zwjWsNbyySMoBSTVJRhTueVs/BpEFM44FMm2j34QNB1InO0RsEZ4a', NULL, 0, 'customer', NULL, '2018-10-28 06:17:59', '2018-10-28 06:17:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uri` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `title`, `slug`, `detail`, `uri`, `created_at`, `updated_at`) VALUES
(1, 'Blins seguridad', 'blins-seguridad', NULL, 'https://www.youtube.com/watch?v=naN6ajm0yyI', '2018-10-17 23:58:13', '2018-10-17 23:58:13'),
(2, 'Blins Suministro de personal', 'blins-suministro-de-personal', NULL, 'https://www.youtube.com/watch?v=HxDj5MrvIVw', '2018-10-17 23:58:31', '2018-10-17 23:58:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `works`
--

CREATE TABLE `works` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(170) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupancy` varchar(145) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_end` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `works`
--

INSERT INTO `works` (`id`, `image`, `title`, `slug`, `area`, `occupancy`, `experience`, `detail`, `salary`, `contact`, `work_end`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Estudiantes de Marketing, Ventas, Atencion al Cliente', 'estudiantes-de-marketing-ventas-atencion-al-cliente', 'Ventas', 'Vendedor', 'Experiencia previa', 'Se ofrece : Capacitacion, Entrenamiento, partidas variables (comisiones), posibilidada de partida fijas por metas.', 'Acorde al sector', '099577533', '2019-02-28', '2018-10-25 20:31:30', '2018-10-25 21:13:43'),
(2, NULL, 'Mucama/o Limpiador/a Auxiliar de Servicios', 'mucama-o-limpiador-a-auxiliar-de-servicios-75820', 'Hotelería - House Keeping', 'Mucama/o', 'Se valorará', 'Ambos Sexos\n18 a 35 años \nPrimaria Completa', 'Acorde al sector', '099577533', '2019-02-28', '2018-10-25 21:24:29', '2018-10-25 21:24:29'),
(3, NULL, 'Limpiador/a Auxiliar de Servicios', 'limpiador-a-auxiliar-de-servicios-71544', 'Hotelería - House Keeping', 'Limpiador/a', 'Se valorará', 'Ambos Sexos\n18 a 35 años\nPrimaria Completa', 'Acorde al sector', '099577533', '2019-02-28', '2018-10-27 02:07:06', '2018-10-27 02:07:06'),
(4, NULL, 'Limpiador Auxiliar de Servicio', 'limpiador-auxiliar-de-servicio-30907', 'Hotelería - Cocina', 'Bachero/a - Limpiador/a', 'Se valorará', 'Ambos Sexos\n18 a 35 años\nPrimaria Completa', 'Acorde al Sector', '099577533', '2019-02-28', '2018-10-27 02:09:04', '2018-10-27 02:09:04'),
(5, NULL, 'Instalador de Alarmas, Instalador de Sistemas Electrónica', 'instalador-de-alarmas-instalador-de-sistemas-electr-nica-73489', 'Seguridad Electrónica', 'Ayudante Técnico', '1 año', 'Masculino\n21 a 40 años\nCiclo Básico', 'Según el sector', '099577533', '2018-11-30', '2018-10-27 02:10:16', '2018-10-27 02:10:16'),
(6, NULL, 'Mozo', 'mozo-88720', 'Alimentos & Bebidas', 'Mozo', 'Se valorará', 'Buena presencia, Disponibilidad y locomoción (se valorará).', 'A convenir', '099577533', '2019-02-28', '2018-10-27 02:11:27', '2018-10-27 02:11:27'),
(7, NULL, 'Cocinero o Ayudante', 'cocinero-o-ayudante-95414', 'Hotelería y Gastronomía', 'Ayudantes y Cocineros', 'Se valorará', 'Experiencia mínima, buena presencia,  cuidar asiduidad y dinámica de trabajo.', 'A acordar', '099577533', '2018-12-31', '2018-10-27 02:12:30', '2018-10-27 02:12:30'),
(8, NULL, 'Auxiliar Comercial, Administrativa, Marketing y Ventas', 'auxiliar-comercial-administrativa-marketing-y-ventas-63681', 'Comercial', 'Auxiliar', 'No excluyente', 'Carga horaria 50 hs. mensuales para inducir en tareas Comerciales y Administrativas. Cobranzas, Promoción, Ventas y Trámites. Dedicación diaria aproximada de lunes a viernes 2 a 4 hs., sábado opcional. Empresa propone Sueldo por hora de acuerdo Consejo de Salarios, Aportes a la Seguridad Social. Capacitación, Entrenamiento, Comunicación, Comisión y Viáticos.', 'Inicio 50 hs por Consejo de Salarios', '099577533', '2018-10-26', '2018-10-27 02:13:31', '2018-10-27 02:13:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `works_has_requests`
--

CREATE TABLE `works_has_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `work_ui` int(10) UNSIGNED DEFAULT NULL,
  `names` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surnames` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupancy` varchar(145) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD UNIQUE KEY `categories_image_unique` (`image`);

--
-- Indices de la tabla `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `links_slug_unique` (`slug`);

--
-- Indices de la tabla `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `messages_slug_unique` (`slug`),
  ADD KEY `messages_user_ui_foreign` (`user_ui`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_slug_unique` (`slug`),
  ADD UNIQUE KEY `news_image_unique` (`image`),
  ADD KEY `news_user_ui_foreign` (`user_ui`);

--
-- Indices de la tabla `news_has_images`
--
ALTER TABLE `news_has_images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_has_images_image_unique` (`image`),
  ADD KEY `news_has_images_news_ui_foreign` (`new_ui`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_category_ui_foreign` (`category_ui`);

--
-- Indices de la tabla `products_has_features`
--
ALTER TABLE `products_has_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_has_features_product_ui_foreign` (`product_ui`);

--
-- Indices de la tabla `products_has_images`
--
ALTER TABLE `products_has_images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_has_images_image_unique` (`image`),
  ADD KEY `products_has_images_product_ui_foreign` (`product_ui`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_url_unique` (`url`),
  ADD UNIQUE KEY `users_token_unique` (`token`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `videos_slug_unique` (`slug`);

--
-- Indices de la tabla `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `works_slug_unique` (`slug`),
  ADD UNIQUE KEY `works_image_unique` (`image`);

--
-- Indices de la tabla `works_has_requests`
--
ALTER TABLE `works_has_requests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `works_has_requests_file_unique` (`file`),
  ADD KEY `works_has_requests_work_ui_foreign` (`work_ui`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `news_has_images`
--
ALTER TABLE `news_has_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products_has_features`
--
ALTER TABLE `products_has_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products_has_images`
--
ALTER TABLE `products_has_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `works`
--
ALTER TABLE `works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `works_has_requests`
--
ALTER TABLE `works_has_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_user_ui_foreign` FOREIGN KEY (`user_ui`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_user_ui_foreign` FOREIGN KEY (`user_ui`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `news_has_images`
--
ALTER TABLE `news_has_images`
  ADD CONSTRAINT `news_has_images_news_ui_foreign` FOREIGN KEY (`new_ui`) REFERENCES `news` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_ui_foreign` FOREIGN KEY (`category_ui`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `products_has_features`
--
ALTER TABLE `products_has_features`
  ADD CONSTRAINT `products_has_features_product_ui_foreign` FOREIGN KEY (`product_ui`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `products_has_images`
--
ALTER TABLE `products_has_images`
  ADD CONSTRAINT `products_has_images_product_ui_foreign` FOREIGN KEY (`product_ui`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `works_has_requests`
--
ALTER TABLE `works_has_requests`
  ADD CONSTRAINT `works_has_requests_work_ui_foreign` FOREIGN KEY (`work_ui`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
