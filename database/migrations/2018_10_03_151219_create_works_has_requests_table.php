<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksHasRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_has_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_ui')->unsigned()->nullable();
            $table->foreign('work_ui')->references('id')->on('works')->onDelete('CASCADE');
            $table->string('names', 75);
            $table->string('surnames', 75);
            $table->string('occupancy', 145);
            $table->string('email', 65);
            $table->string('phone', 16)->nullable();
            $table->string('file', 190)->unique();
            $table->string('image', 190)->unique()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works_has_requests');
    }
}
