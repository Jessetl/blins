<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsHasImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_has_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_ui')->unsigned();
            $table->foreign('product_ui')->references('id')->on('products')->onDelete('CASCADE');
            $table->string('image', 190)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_has_images');
    }
}
