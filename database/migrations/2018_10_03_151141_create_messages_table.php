<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_ui')->unsigned()->nullable();
            $table->foreign('user_ui')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('title', 175);
            $table->string('slug', 190)->unique();
            $table->string('email', 95);
            $table->string('names', 65);
            $table->string('surnames', 65);
            $table->string('phone', 35);
            $table->longText('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
