<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image', 170)->unique()->nullable();
            $table->string('title', 175);
            $table->string('slug', 190)->unique();
            $table->string('area', 125);
            $table->string('occupancy', 145);
            $table->string('experience', 65);
            $table->longText('detail');
            $table->string('salary', 175);
            $table->string('contact');
            $table->date('work_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
