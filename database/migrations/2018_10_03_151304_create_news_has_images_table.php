<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsHasImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_has_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('new_ui')->unsigned();
            $table->foreign('new_ui')->references('id')->on('news')->onDelete('CASCADE');
            $table->string('image', 190)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_has_images');
    }
}
