<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 175)->unique()->nullable();
            $table->string('names', 65);
            $table->string('surnames', 65);
            $table->string('email', 35)->unique();
            $table->string('password', 190);
            $table->string('token', 175)->unique()->nullable();
            $table->boolean('token_confirmed')->default(0);
            $table->enum('type', array('root', 'administrator', 'customer'))->default('customer');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
